
var app = new Vue({
    el: "#app",
    data: {
        contacts : [],
        page: 1,
        perPage: 4,
        pages: [],
        newName: '',
        newPhone: '',
    },
    methods:{
        getPosts() {	
            axios.get('controller.php')
            .then(function (response) {
                app.contacts = response.data;
            })
            .catch(e => {
                this.errors.push(e)
            });
        },
        setPages() {
            let numberOfPages = Math.ceil(this.contacts.length / this.perPage);
            for (let index = 1; index <= numberOfPages; index++) {
                this.pages.push(index);
            }
            if ( this.page > numberOfPages ) this.page = numberOfPages;
        },
        paginate(contacts) {
            let page = this.page;
            let perPage = this.perPage;
            let from = (page * perPage) - perPage;
            let to = (page * perPage);
            return  contacts.slice(from, to);
        },
        create() {
            this.newName = this.newName.trim();
            if ( this.newName != '' && this.newPhone.length == 15 ) {
                let data = new FormData();
                data.append('type', 'create');
                data.append('name', this.newName);
                data.append('phone', this.newPhone);
                axios.post('controller.php', data)
                .then(response => {
                    this.filter = response.data
                    this.pages = [];
                    this.page = 1;
                    this.getPosts();
                    this.newName = this.newPhone = '';
                })
                .catch(e => {
                    this.errors.push(e)
                });
            }
        },
        delContact(id) {
            var data = new FormData();
            data.append('type', 'delete');
            data.append('id', id);
            axios.post('controller.php', data)
                .then(response => {
                    this.pages = [];
                    this.getPosts();
                })
                .catch(e => {
                    this.errors.push(e)
                });
        },
        formatPhone(phone) {
            let fPhone = phone.replace(/\D/g,'');
            if (!fPhone) return '';
            let dig = fPhone.replace(/\D/g,'').match(/(\d)(\d{0,3})(\d{0,3})(\d{0,2})(\d{0,2})/);
            fPhone = dig[1] + ' ' + dig[2] + ' ' + dig[3] + ' ' + dig[4] + ' ' + dig[5];
            return fPhone.trim();
            
        },
        acceptPhone() {
            this.newPhone = this.formatPhone(this.newPhone);
        },
    },
    computed: {
        displayedContacts() {
            return this.paginate(this.contacts);
        }
    },
    watch: {
        contacts() {
            this.setPages();
        }
        },
        created() {
            this.getPosts();
    },
})
