<?php
include "config.php";

if ( isset($_POST['type']) ) {
    switch ($_POST['type']) {
        case 'create':
            if ( isset($_POST['name']) && isset($_POST['phone']) ) {
                $name = mysqli_real_escape_string($con, trim($_POST['name']));
                $phone = mysqli_real_escape_string($con, preg_replace('/[^\d]/', '', trim($_POST['phone'])));
                if ( !$name || !$phone ) exit;
                $req = 'INSERT INTO `contacts` (`name`, `phone`) VALUES( "' 
                . $name . '", "' . $phone . '")';
                $userData = mysqli_query( $con, $req );
            }
            break;
        case 'delete':
            if ( isset($_POST['id']) ) {
                $req = 'DELETE FROM `contacts` WHERE `id` = ' . (int)$_POST['id'];
                $userData = mysqli_query( $con, $req );
            }
            break;
    }
} else {
    $data = mysqli_query( $con, "SELECT * FROM `contacts` ORDER BY `id` DESC" );
    $response = array(); 
    while( $row = mysqli_fetch_assoc($data) ) {
        $response[] = $row;
    }
    echo json_encode($response);
}
exit;
