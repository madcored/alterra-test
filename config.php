<?php

$host = "localhost";
$user = "root";
$password = "root";
$dbname = "alterra";

$con = mysqli_connect($host, $user, $password,$dbname);

if (!$con) {
    error_log(("Connection failed: " . mysqli_connect_error()));
}
